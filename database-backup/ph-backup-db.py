#!/usr/bin/env python3

# Script to backup PostgresQL database and upload to Yandex Disk.
# The name of database is passed via command line argument. The login
# credentials are passed as environment variables YANDEX_USERNAME, YANDEX_PASSWORD
#
# How to run:
# YANDEX_USERNAME=user@yandex.ru YANDEX_PASSWORD=1234 ./ph-backup-db.py mydb

import os
import shutil
import os.path
import tarfile
from os.path import basename
from subprocess import Popen, PIPE, CalledProcessError, check_call
from pathlib import PurePosixPath
from datetime import datetime
from tempfile import NamedTemporaryFile

import click
import webdav.client as wc

YANDEX_PATH = 'Backup/DB/'


def dump_database(db_name):
    db_file = NamedTemporaryFile()
    # Implement the command `pg_dump -O db_name | gzip -c > /tmp/somefile`
    p_pg = Popen(['pg_dump', '-O', db_name], stdout=PIPE)
    p_gz = Popen(['gzip', '-c'], stdin=p_pg.stdout, stdout=db_file)
    p_pg.stdout.close()
    p_gz.wait()
    return db_file

def dump_ts_database(db_name):
    # Time-series database is dumped to /tmp/ts_name_20170130-000000
    now = datetime.now()
    folder_path = '/tmp/ts_{}_{:%Y%m%d-%H%M%S}'.format(db_name, now)
    if os.path.exists(folder_path):
        shutil.rmtree(folder_path)
    os.mkdir(folder_path)
    try:
        check_call(['influxd', 'backup', '-database', db_name, folder_path])
    except CalledProcessError:
        return False
    # Done dump. Compress
    tarpath = folder_path + '.tar'
    with tarfile.open(tarpath, 'w') as tar:
        tar.add(folder_path)
    # Remove dump folder
    shutil.rmtree(folder_path)
    return tarpath

def create_remote_directory(client, path):
    # `path` will be db_name/year/month/day/
    # `real_path` will be Backup/DB/db_name/year/month/day/
    real_path = os.path.join(YANDEX_PATH, path)
    click.echo('Does {} exist?'.format(real_path))
    # First we test if Backup/DB/db_name/year/month/day/ exist.
    exist = client.check(real_path)
    click.echo(exist)
    if exist:
        return
    # If not, we create the subfolders
    p = PurePosixPath(path)
    skip_check = False
    for i in range(len(p.parts)):
        sub = os.path.join(YANDEX_PATH, '/'.join(p.parts[:i+1]) + '/')
        # Checking for directory existence is slow, so we don't have to
        # check the subpaths after creating
        exist = client.check(sub) if not skip_check else False
        if exist:
            continue
        click.echo('Create {}'.format(sub))
        client.mkdir(sub)
        skip_check = True


@click.command()
@click.argument('db_name')
@click.argument('yandex_username', envvar='YANDEX_USERNAME')
@click.argument('yandex_password', envvar='YANDEX_PASSWORD')
def main(db_name, yandex_username, yandex_password):
    options = {
        'webdav_hostname': 'https://webdav.yandex.ru',
        'webdav_login': yandex_username,
        'webdav_password': yandex_password
    }
    client = wc.Client(options)
    now = datetime.now()
    # Dump database to /tmp/
    click.echo('Dump database {}'.format(db_name))
    db_file = dump_database(db_name)
    ts_file = dump_ts_database(db_name)
    # The dump file is placed under subdirectory name/2016/01/30
    subdir = '{}/{:%Y/%m/%d/}'.format(db_name, now)
    # Create directory on Yandex Disk
    create_remote_directory(client, subdir)
    # Upload to Yandex Disk
    # Upload relational database
    filepath = db_file.name
    # The dump file will be named like `name_20160130-000000.sql.gz`
    remote_name = '{}_{:%Y%m%d-%H%M%S}.sql.gz'.format(db_name, now)
    remote_path = os.path.join(YANDEX_PATH, subdir, remote_name)
    click.echo('Upload {} to {}'.format(filepath, remote_path))
    client.upload_sync(remote_path=remote_path, local_path=filepath)
    # Upload time-series database
    remote_path = os.path.join(YANDEX_PATH, subdir, basename(ts_file))
    client.upload_sync(remote_path=remote_path, local_path=ts_file)
    # Delete ts database dump file
    os.remove(ts_file)
    click.echo('Done. Bye!')


if __name__ == '__main__':
    main()
